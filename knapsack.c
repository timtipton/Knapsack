//Name: Tim Tipton
//Class: CSE 222
//Instructor: Nick Macias
//Date: 3/3/2021
//Assignment: PA5
//Overview: This program reads information from the command line to get the capacity of a virfual 
//knapsack and a text file if different items to put into that sack and tells you what the maximum 
//value of items you can fit in that knapsack and what they are.


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct answer{
   int item_counts[128];
   int max_value;
};

char name[128][33];
int item_value[128]={0};
int item_weight[128]={0};
struct answer cache[1024];
struct answer tempValue;
int array_length=sizeof(item_value)/sizeof(item_value[0]);
int cap;
struct answer maxVal(int capacity);

void main(int argc, char *argv[]){

   sscanf(argv[1], "%d", &cap);


   struct answer myVal;
   for(int i=0; i<=128;i++){
      myVal.item_counts[i]=0;
   }
   myVal.max_value=0;
   if(argc==3){
      FILE *fp=fopen(argv[2], "r");
      char buffer[120], item_name[35];
      int value, weight;
      fgets(buffer, 120, fp);
      int s_return=sscanf(buffer, "%s %d %d", item_name, &value, &weight);

      if(fp==NULL){
         printf("ERROR: Nonexistent data file\n");
      }
      else{
         if(s_return!=3){
            printf("ERROR: Invalid data file\n");
         }
         else{
            int i=0;
            strcpy(name[i], item_name);
            item_value[i]=value;
            item_weight[i]=weight;
            printf("%s  %d  %d\n", name[i], item_value[i], item_weight[i]);
            while(fgets(buffer, 120, fp)!=NULL){
               sscanf(buffer, "%s %d %d", item_name, &value, &weight);
                  strcpy(name[i], item_name);
                  item_value[i]=value;
                  item_weight[i]=weight;
                  printf("%s  %d  %d\n", name[i], item_value[i], item_weight[i]);
                  i++;
            }

for(int i=0; i<=5; i++){
printf("%d is weight, %d is value\n", item_weight[i], item_value[i]);}

            myVal=maxVal(cap);
            printf("Bag's Capacity=%d\n", cap);
            printf("Highest possible value=%d\n", myVal.max_value);
            for(int i; i<=array_length; i++){
               if(myVal.item_counts[i]!=0){
                  printf("Item %d (%s): %d\n", i, name[i], myVal.item_counts[i]);
               }
            }


         }
      }
   }
   else{
      printf("ERROR: Wrong number of command line arguments\n");
   }  
return;
}

struct answer maxVal(int capacity){
   int i=0;
   struct answer bestValue;
   struct answer tempValue;
   int array_length=sizeof(item_value)/sizeof(item_value[0]);
   int cap=capacity;
   int best=0;


   if(cache[cap].max_value!=0){
      return cache[cap];
   }
   else{

   for(int i=0; i<=array_length;i++){
      bestValue.item_counts[i]=0;
      tempValue.item_counts[i]=0;
   }
   bestValue.max_value=0;
   tempValue.max_value=0;
}
   if(cap<=0) return bestValue;
   else{
   for(int i=0; i<=array_length; i++){
      if(capacity>=item_weight[i]){
         tempValue=maxVal(cap-item_weight[i]);
         if(tempValue.max_value+item_value[i]>bestValue.max_value){
            bestValue=tempValue;
            bestValue.max_value=tempValue.max_value+item_value[i];
            ++bestValue.item_counts[i];
         }
cache[cap]=bestValue;
return bestValue;
      }
  } }
}



